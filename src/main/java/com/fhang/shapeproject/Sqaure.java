/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shapeproject;

/**
 *
 * @author Natthakritta
 */
public class Sqaure extends Shape{
    private double side;

    public Sqaure(double side) {
        super("Sqaure");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    
    @Override
    public double calArea() {
        return side*side;
    }

    @Override
    public double calPerimeter() {
       return side*4;
    }
    
}
