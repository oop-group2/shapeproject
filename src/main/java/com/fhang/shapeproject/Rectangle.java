/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shapeproject;

/**
 *
 * @author Natthakritta
 */
public class Rectangle extends Shape {

    private double width;
    private double high;

    public Rectangle(double width, double high) {
        super("Rectangle");
        this.width = width;
        this.high = high;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    @Override
    public double calArea() {
        return width * high;
    }

    @Override
    public double calPerimeter() {
        return (width * 2) + (high * 2);
    }

}
