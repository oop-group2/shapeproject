/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Natthakritta
 */
public class TriangleFrame extends JFrame {

    JLabel lblSide;
    JTextField txtSide;
    JLabel lblHigh;
    JTextField txtHigh;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Rectangle");
        this.setSize(410, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(40, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField();
        txtSide.setSize(40, 20);
        txtSide.setLocation(50, 5);
        this.add(txtSide);

        lblHigh = new JLabel("high:", JLabel.TRAILING);
        lblHigh.setSize(40, 20);
        lblHigh.setLocation(100, 5);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        this.add(lblHigh);

        txtHigh = new JTextField();
        txtHigh.setSize(40, 20);
        txtHigh.setLocation(150, 5);
        this.add(txtHigh);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(200, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle side = ??? high = ??? arae = ??? perimeter = ???");
        //  lblResult.setHorizontalTextPosition(JLabel.CENTER);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 40);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // System.out.println("Clicked");    
                    // 1. txtSide -> String 
                    //    txtHigh -> String 
                    String strSide = txtSide.getText();
                    String strHigh = txtHigh.getText();
                    // 2. strSide -> Side:  double
                    //    strHigh -> high:  double
                    Double side = Double.parseDouble(strSide);
                    Double high = Double.parseDouble(strHigh);
                    // 3. instace object Triangle (side,high) -> triangle
                    Triangle  triangle = new  Triangle(side, high);
                    // 4. update lblResult 
                    lblResult.setText(" Triangle side = " + String.format("%.2f",  triangle.getSide()) + " high = "
                            + String.format("%.2f", triangle.getHigh()) + " arae =  " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    // System.out.println("ERROR!!!");
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtHigh.setText("");
                    txtSide.requestFocus();
                    txtHigh.requestFocus();

                }
            }

        }
        );
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
