/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Natthakritta
 */
public class SqaureFrame extends JFrame {

    JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;

    public SqaureFrame() {
        super("Sqaure");
        this.setSize(310, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Sqaure side = ??? arae = ??? perimeter = ???");
        //  lblResult.setHorizontalTextPosition(JLabel.CENTER);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 40);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//Anonymous class
                try {
                    String strSide = txtSide.getText();
                    Double side = Double.parseDouble(strSide);
                    Sqaure sqaure = new  Sqaure(side);
                    lblResult.setText("Sqaure side = " + String.format("%.2f", sqaure.getSide())
                            + " arae = " + String.format("%.2f", sqaure.calArea()) + " perimeter = " + String.format("%.2f", sqaure.calPerimeter()));
                } catch (Exception ex) {
                    // System.out.println("ERROR!!!");
                    JOptionPane.showMessageDialog(SqaureFrame.this, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        SqaureFrame frame = new SqaureFrame();
        frame.setVisible(true);
    }
}
