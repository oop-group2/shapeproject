/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shapeproject;

/**
 *
 * @author Natthakritta
 */
public class Triangle extends Shape{
    private double side;
    private double high;

    public Triangle(double side, double high) {
        super("Triangle");
        this.side = side;
        this.high = high;
    }

    public double getSide() {
        return side;
    }

    public double getHigh() {
        return high;
    }
    
    @Override
    public double calArea() {
        return 0.5*side*high;
    }

    @Override
    public double calPerimeter() {
      return side*3;
    }
    
}
