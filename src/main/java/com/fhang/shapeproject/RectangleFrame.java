/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shapeproject;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Natthakritta
 */
public class RectangleFrame extends JFrame {

    JLabel lblWidth;
    JTextField txtWidth;
    JLabel lblHigh;
    JTextField txtHigh;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(410, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(40, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        txtWidth = new JTextField();
        txtWidth.setSize(40, 20);
        txtWidth.setLocation(50, 5);
        this.add(txtWidth);

        lblHigh = new JLabel("high:", JLabel.TRAILING);
        lblHigh.setSize(40, 20);
        lblHigh.setLocation(100, 5);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        this.add(lblHigh);

        txtHigh = new JTextField();
        txtHigh.setSize(40, 20);
        txtHigh.setLocation(150, 5);
        this.add(txtHigh);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(200, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width = ??? high = ??? arae = ??? perimeter = ???");
        //  lblResult.setHorizontalTextPosition(JLabel.CENTER);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 40);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // System.out.println("Clicked");    
                    // 1. txtWidth -> String 
                    //    txtHigh -> String 
                    String strWidth = txtWidth.getText();
                    String strHigh = txtHigh.getText();
                    // 2. strWidth -> width:  double
                    //    strHigh -> high:  double
                    Double width = Double.parseDouble(strWidth);
                    Double high = Double.parseDouble(strHigh);
                    // 3. instace object Rectangle(width,high) -> rectangle
                    Rectangle rectangle = new Rectangle(width, high);
                    // 4. update lblResult 
                    lblResult.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth()) + " high = "
                            + String.format("%.2f", rectangle.getHigh()) + " arae =  " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    // System.out.println("ERROR!!!");
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHigh.setText("");
                    txtWidth.requestFocus();
                    txtHigh.requestFocus();

                }
            }

        }
        );
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
